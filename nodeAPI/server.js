const express = require('express')
const app = express()
const MongoClient = require('mongodb')
  .MongoClient;
const assert = require('assert');
const config = require("./config.json")
// Use connect method to connect to the server
app.get('/', (req, res) => res.send('Hello Mom!'))
app.get("/codedeployed/:uuid/:username/:code", async(req, res) => {
  var client = await MongoClient.connect(config.dburl, {
    authSource: "admin"
  });
  var db = client.db("minecraft");
  var codes = db.collection("codesGiven");
  await codes.insertOne({
    uuid: req.params.uuid,
    username: req.params.username,
    code: req.params.code,
    redeemed: false
  });
  console.log("Dispatched code " + req.params.code + " for " + req.params.username)
  res.json({
    success: true
  })
})
app.get("/redeem/:code", async(req, res) => {
  var client = await MongoClient.connect(config.dburl, {
    authSource: "admin"
  });
  var db = client.db("minecraft");
  var codes = db.collection("codesGiven");
  var found = await codes.findOne({
    code: req.params.code
  });
  if (!found) return res.json({
    success: false,
    error: "Invalid code."
  })
  if (found.redeemed) return res.json({
    success: false,
    error: "Code already redeemed"
  })
  await codes.findOneAndUpdate({
    code: req.params.code
  }, {
    $set: {
      redeemed: true
    }
  })
  res.json({
    success: true,
    username: found.username,
    uuid: found.uuid
  })
})
app.listen(5544, () => console.log(JSON.stringify({
  status: "listening",
  port: 5544
})))
